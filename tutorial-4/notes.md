# Singleton Pattern 

## Lazy Approach
Instance objectnya diinisialisasi saat diperlukan saja. 

Pros: 
- Object hanya dibuat saat diperlukan, mengurangi CPU time berlebih
- Memungkinkan exception handling

Cons:
- Setiap kali dipanggil isInstance(), harus dicek nilai null
- Instance nya tidak bisa diakses secara langsung
- Dalam environment multithreaded, bisa melanggar sifat singleton

## Eager Approach
Instance diinisialisasi saat class nya pertama dicompile. 

Pros: 
- Mudah diimplementasikan

Cons:
- Instance class selalu dibuat meski dipakai atau tidak 
- CPU time terbuang untuk membuat instance meski tidak diperlukan
- Tidak memungkinkan exception handling.

Sumber: [geeksforgeeks](https://www.geeksforgeeks.org/java-singleton-design-pattern-practices-examples/)