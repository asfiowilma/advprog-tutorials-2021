package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class Menu {
    private String name;
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;
    private IngridientFactory factory;

    //To Do : Complete Me
    //Silahkan tambahkan parameter jika dibutuhkan
    public Menu(String name, IngridientFactory factory){
        this.name = name;
        this.factory = factory;

        prepare();
    }

    public String getName() {
        return name;
    }

    public Noodle getNoodle() {
        return noodle;
    }

    public Meat getMeat() {
        return meat;
    }

    public Topping getTopping() {
        return topping;
    }

    public Flavor getFlavor() {
        return flavor;
    }

    public void prepare() {
        flavor = factory.createFlavor();
        meat = factory.createMeat();
        noodle = factory.createNoodle();
        topping = factory.createTopping();
    }
}