package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderServiceImplTest {
    private OrderServiceImpl orderService = new OrderServiceImpl();

    @Test
    public void testOrderADrink() {
        orderService.orderADrink("yey");
        assertEquals("yey", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderAFood() {
        orderService.orderAFood("whoa");
        assertEquals("whoa", orderService.getFood().getFood());
    }

}