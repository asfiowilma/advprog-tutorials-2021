package id.ac.ui.cs.advprog.tutorial4;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Tutorial4ApplicationTest {
    @Test
    void contextLoads() {
    }

    //Only to cover main() invocation.
    @Test
    public void main() {
        Tutorial4Application.main(new String[] {});
    }

}
