package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IngridientsTest {

    // FLAVOR
    @Test
    public void testSweetDesc() {
        Sweet sweet = new Sweet();
        assertEquals("Adding a dash of Sweet Soy Sauce...", sweet.getDescription());
    }
    @Test
    public void testSpicyDesc() {
        Spicy spicy = new Spicy();
        assertEquals("Adding Liyuan Chili Powder...", spicy.getDescription());
    }
    @Test
    public void testUmamiDesc() {
        Umami umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }
    @Test
    public void testSaltyDesc() {
        Salty salty = new Salty();
        assertEquals("Adding a pinch of salt...", salty.getDescription());
    }

    // NOODLES
    @Test
    public void testRamenDesc() {
        Ramen ramen = new Ramen();
        assertEquals("Adding Inuzuma Ramen Noodles...", ramen.getDescription());
    }
    @Test
    public void testShiratakiDesc() {
        Shirataki shirataki = new Shirataki();
        assertEquals("Adding Snevnezha Shirataki Noodles...", shirataki.getDescription());
    }
    @Test
    public void testSobaDesc() {
        Soba soba = new Soba();
        assertEquals("Adding Liyuan Soba Noodles...", soba.getDescription());
    }
    @Test
    public void testUdonDesc() {
        Udon udon = new Udon();
        assertEquals("Adding Mondo Udon Noodles...", udon.getDescription());
    }

    // MEAT
    @Test
    public void testPorkDesc() {
        Pork pork = new Pork();
        assertEquals("Adding Tian Xu Pork Meat...", pork.getDescription());
    }
    @Test
    public void testFishDesc() {
        Fish fish = new Fish();
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }
    @Test
    public void testBeefDesc() {
        Beef beef = new Beef();
        assertEquals("Adding Maro Beef Meat...", beef.getDescription());
    }
    @Test
    public void testChickenDesc() {
        Chicken chicken = new Chicken();
        assertEquals("Adding Wintervale Chicken Meat...", chicken.getDescription());
    }

    // TOPPING
    @Test
    public void testBoiledEggDesc() {
        BoiledEgg boiledEgg = new BoiledEgg();
        assertEquals("Adding Guahuan Boiled Egg Topping", boiledEgg.getDescription());
    }
    @Test
    public void testCheeseDesc() {
        Cheese cheese = new Cheese();
        assertEquals("Adding Shredded Cheese Topping...", cheese.getDescription());
    }
    @Test
    public void testFlowerDesc() {
        Flower flower = new Flower();
        assertEquals("Adding Xinqin Flower Topping...", flower.getDescription());
    }
    @Test
    public void testMushroomDesc() {
        Mushroom mushroom = new Mushroom();
        assertEquals("Adding Shiitake Mushroom Topping...", mushroom.getDescription());
    }
}