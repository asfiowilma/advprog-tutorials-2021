package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MenuTest {
    private Menu menu = new SnevnezhaShirataki("shira");

    @Test
    public void testGetName() {
        assertEquals("shira", menu.getName());
    }

    @Test
    public void testGetNoodle() {
        assertTrue(menu.getNoodle() instanceof Shirataki);
    }
    @Test
    public void testGetTopping() {
        assertTrue(menu.getTopping() instanceof Flower);
    }
    @Test
    public void testGetFlavor() {
        assertTrue(menu.getFlavor() instanceof Umami);
    }
    @Test
    public void testGetMeat() {
        assertTrue(menu.getMeat() instanceof Fish);
    }
}