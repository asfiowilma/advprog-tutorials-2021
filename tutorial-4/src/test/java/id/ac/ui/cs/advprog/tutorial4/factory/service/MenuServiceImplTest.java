package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MenuServiceImplTest {
    @Spy
    private MenuRepository repository;

    @InjectMocks
    private MenuServiceImpl menuService;

    @BeforeEach
    public void init() {
        repository.getMenus().clear();
    }

    @Test
    public void testInitClass() {
        menuService = new MenuServiceImpl();
        verify(repository, times(4)).add(any(Menu.class));
    }

    @Test
    public void testGetMenus() {
        List<Menu> mockMenu = new ArrayList<>();
        mockMenu.add(new LiyuanSoba("soba"));
        mockMenu.add(new InuzumaRamen("ramen"));

        when(repository.getMenus()).thenReturn(mockMenu);

        List<Menu> menus = menuService.getMenus();
        verify(repository, atLeast(1)).getMenus();
        assertEquals(mockMenu, menus);
    }

    @Test
    public void testCreateMenu() {
        menuService.createMenu("inu", "InuzumaRamen");
        verify(repository, atLeast(1)).add(any(InuzumaRamen.class));
        assertEquals(1, repository.getMenus().size());
    }
}