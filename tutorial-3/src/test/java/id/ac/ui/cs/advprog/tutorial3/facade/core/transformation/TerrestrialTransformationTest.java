package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TerrestrialTransformationTest {
    private Class<?> terrestrialClass;

    @BeforeEach
    public void setup() throws Exception {
        terrestrialClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.TerrestrialTransformation");
    }

    @Test
    public void testTerrestrialHasEncodeMethod() throws Exception {
        Method translate = terrestrialClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTerrestrialEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "VdiludCdqgCLCzhqwCwrCdCeodfnvplwkCwrCirujhCrxuCvzrug";

        Spell result = new TerrestrialTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testTerrestrialEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";

        Spell result = new TerrestrialTransformation(5).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testTerrestrialHasDecodeMethod() throws Exception {
        Method translate = terrestrialClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTerrestrialDecodesCorrectly() throws Exception {
        String text = "VdiludCdqgCLCzhqwCwrCdCeodfnvplwkCwrCirujhCrxuCvzrug";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new TerrestrialTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testTerrestrialDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "XfknwfEfsiENE1jsyEytEfEgqfhpxrnymEytEktwljEtzwEx1twi";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new TerrestrialTransformation(5).decode(spell);
        assertEquals(expected, result.getText());
    }
}
