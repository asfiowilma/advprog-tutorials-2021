package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.*;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {
    private boolean adapted;

    @Autowired
    private LogRepository logs;
    @Autowired
    private WeaponRepository weapons;
    @Autowired
    private BowRepository bows;
    @Autowired
    private SpellbookRepository spellbooks;

//    public void seed() {
//        Weapon angewomon = new BowAdapter(new IonicBow("Angewomon"));
//        Weapon angemon = new BowAdapter(new UranosBow("Angemon"));
//        weapons.save(angemon);
//        weapons.save(angewomon);
//
//        Weapon agumon = new SpellbookAdapter(new Heatbearer("Agumon"));
//        Weapon piyomon = new SpellbookAdapter(new TheWindjedi("Piyomon"));
//        weapons.save(agumon);
//        weapons.save(piyomon);
//
//        Weapon palmon = new FesteringGreed("Palmon");
//        Weapon garurumon = new FullMoonPike("Garurumon");
//        Weapon gomamon = new SeawardPride("Gomamon");
//        Weapon tentomon = new StaffOfHoumo("Tentomon");
//        weapons.save(palmon);
//        weapons.save(garurumon);
//        weapons.save(gomamon);
//        weapons.save(tentomon);
//    }

    @Override
    public List<Weapon> findAll() {
        if (!adapted) {
            for (Bow bow: bows.findAll()) {
                weapons.save(new BowAdapter(bow));
            }
            for (Spellbook spellbook : spellbooks.findAll()) {
                weapons.save(new SpellbookAdapter(spellbook));
            }
            adapted = true;
        }
        return weapons.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon w = weapons.findByAlias(weaponName);
        if (attackType == 0) logs.addLog(w.getHolderName() + " attacced with" + w.getName() + ": " + w.normalAttack());
        else logs.addLog(w.getHolderName() + " attacced with" + w.getName() + ": " + w.chargedAttack());
        weapons.save(w);
    }

    @Override
    public List<String> getAllLogs() {
        return logs.findAll();
    }
}
