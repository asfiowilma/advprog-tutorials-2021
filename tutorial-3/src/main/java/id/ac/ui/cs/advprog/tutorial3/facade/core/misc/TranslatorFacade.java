package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.*;

import java.util.*;

public class TranslatorFacade {
    static List<Transformation> transformations;
    static AlphaCodex a;
    static RunicCodex r;

    public TranslatorFacade() {
        transformations = new ArrayList<>();
        transformations.add(new CelestialTransformation());
        transformations.add(new AbyssalTransformation());
        transformations.add(new TerrestrialTransformation());
        a = AlphaCodex.getInstance();
        r = RunicCodex.getInstance();
    }

    public String encode(String text) {
        Spell spell = new Spell(text, a);
        for (Transformation tr : transformations) {
            spell = tr.encode(spell);
        }
        spell = CodexTranslator.translate(spell, r);

        return spell.getText();
    }

    public String decode(String code) {
        Spell spell = new Spell(code, r);
        spell = CodexTranslator.translate(spell, a);
        Collections.reverse(transformations);
        for (Transformation tr : transformations) {
            spell = tr.decode(spell);
        }
        Collections.reverse(transformations);
        return spell.getText();
    }
}
