package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        return this.spellbook.largeSpell();
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
