package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aim; // false = mode spontan; true = mode aim shot

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.aim = false;
    }

    @Override
    public String normalAttack() {
        return this.bow.shootArrow(aim);
    }

    @Override
    public String chargedAttack() {
        this.aim = !aim;
        return aim ? "Entering aim shot mode" : "Leaving aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
