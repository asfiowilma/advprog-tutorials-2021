package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
* Kelas ini mengimplementasikan sistem kriptografi Caesar Cipher
*/
public class TerrestrialTransformation implements Transformation {
    int key;

    public TerrestrialTransformation(int key) {
        this.key = key;
    }

    public TerrestrialTransformation() { this(3); }

    @Override
    public Spell encode(Spell spell) { return process(spell, true); }

    @Override
    public Spell decode(Spell spell) { return process(spell, false); }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int shifter = encode ? key : key * -1;

        int n = text.length();
        char[] res = new char[n];
        for (int i = 0; i < n; i++) {
            char ch = text.charAt(i);
            int idxRaw = codex.getIndex(ch) + shifter; // may cause indexoutofbounds
            int idx = idxRaw >= codex.getCharSize() ? idxRaw - codex.getCharSize()
                    : idxRaw < 0 ? idxRaw + codex.getCharSize() : idxRaw;
            res[i] = codex.getChar(idx);
        }

        return new Spell(new String(res), codex);
    }
}
