# Requirements

## Mahasiswa & Mata Kuliah

- Mahasiswa bisa melamar pada lowongan yang tersedia
- Satu mata kuliah bisa menerima banyak mahasiswa
- Mahasiswa hanya bisa menjadi asisten di satu mata kuliah
  - Mahasiswa hanya bisa mendaftar ke satu mata kuliah 
  - Anggap semua yang mendaftar ke lowongan langsung diterima
- Mata kuliah yang sudah punya asisten tidak bisa dihapus
- Mahasiswa yang sudah jadi asisten tidak bisa dihapus

## Log

- Mahasiswa bisa mempunyai banyak log untuk suatu mata kuliah
  - Log bisa dibuat (ditambah baru)
  - Diperbaharui
  - Dihapus
- Log setiap bulan dirangkum menjadi sebuah laporan pembayaran, berisi:
  - Data bulan
  - Jam kerja dengan satuan jam
  - Jumlah uang yang diperoleh dalam satuan Greil   
  - Semua pekerjaan asisten pada bulan tertentu
- Tarif asisten adalah 350 Greil/jam
- Log bisa berisi data lebih singkat dari satu jam 

# Tables

## MataKuliah
```
KodeMatkul: String (Primary Key)
namaMatkul: String
prodi: String
```
Sample query
```json
{
  "kodeMatkul" : "SDA",
  "nama" : "Struktur Data & Algoritma",
  "prodi" : "Ilmu Komputer / Sistem Informasi"
}
```
## Mahasiswa
```
Mahasiswa

NPM: String (Primary Key)
Nama : String
Email: String
ipk: String
noTelp: String
```
Sample query
```json
{
  "npm" : "1906950944",
  "nama" : "Asfiolitha Wilmarani",
  "email" : "asfiolitha.wilmarani@ui.ac.id",
  "ipk" : "4.0",
  "noTelp" : "123456"
}
```
## Log
```
idLog: integer(Primary Key, auto increment)
startTime: datetime 
endTime: datetime
description: Text
```
> Datetime standard form: `yyyy-MM-dd'T'HH:mm:ss`
> 
Sample query
```json
{
  "startTime" : "2021-05-04 00:00:00",
  "endTime" : "2021-05-04 04:00:00",
  "description" : "Ngoreksi Lab02"
}
```