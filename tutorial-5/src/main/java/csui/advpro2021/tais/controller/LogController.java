package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/log")
public class LogController {

    @Autowired
    private LogService logService;
    @Autowired
    private MahasiswaService mahasiswaService;

    @PostMapping(path="/asdos/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createLog(@PathVariable String npm, @RequestBody Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        log.setMahasiswa(mahasiswa);
        return ResponseEntity.ok(logService.createLog(log));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogByIdLog(@PathVariable int id) {
        Log log = logService.getLogByIdLog(id);
        return log == null ? new ResponseEntity(HttpStatus.NOT_FOUND) : ResponseEntity.ok(log);
    }

    @GetMapping(path = "/asdos/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Log>> getAllLogsByMahasiswa(@PathVariable String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(logService.getAllLogsByMahasiswa(mahasiswa));
    }

    @GetMapping(path = "/asdos/{npm}/{month}-{year}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getMonthlyLogsByMahasiswa(@PathVariable String npm, @PathVariable int month, @PathVariable int year) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(logService.getMonthlyLogsByMahasiswa(mahasiswa, month, year));
    }

    @GetMapping(path = "/asdos/{npm}/laporan/{month}-{year}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLaporanByMahasiswa(@PathVariable String npm, @PathVariable int month, @PathVariable int year) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(logService.getLaporanPembayaranByMahasiswa(mahasiswa, month, year));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity updateLog(@PathVariable int id, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(id, log));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteLogByIdLog(@PathVariable int id) {
        logService.deleteLogByIdLog(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
