package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

public interface LogService {
    Log createLog(Log log);

    Iterable<Log> getListLog();

    Log getLogByIdLog(Integer idLog);

    List<Log> getAllLogsByMahasiswa(Mahasiswa mahasiswa);

    List<Log> getMonthlyLogsByMahasiswa(Mahasiswa mahasiswa, int month, int year);

    LaporanPembayaran getLaporanPembayaranByMahasiswa(Mahasiswa mahasiswa, int month, int year);

    Log updateLog(int idLog, Log log);

    void deleteLogByIdLog(Integer idLog);
}
