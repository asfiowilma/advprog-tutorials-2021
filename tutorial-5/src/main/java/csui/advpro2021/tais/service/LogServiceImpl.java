package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Service
public class LogServiceImpl implements LogService{

    @Autowired
    private LogRepository logRepository;

    @Override
    public Log createLog(Log log) {
        return logRepository.save(log);
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Log getLogByIdLog(Integer idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public List<Log> getAllLogsByMahasiswa(Mahasiswa mahasiswa) {
        return logRepository.findByMahasiswa(mahasiswa);
    }

    @Override
    public List<Log> getMonthlyLogsByMahasiswa(Mahasiswa mahasiswa, int month, int year) {
        LocalDate init = LocalDate.of(year, month, 2);
        LocalDate start = init.withDayOfMonth(1);
        LocalDate end = init.withDayOfMonth(init.lengthOfMonth());
        return logRepository.findByStartTimeBetween(convertToDateViaSqlDate(start), convertToDateViaSqlDate(end));
    }

    @Override
    public LaporanPembayaran getLaporanPembayaranByMahasiswa(Mahasiswa mahasiswa, int month, int year) {
        List<Log> logs = getMonthlyLogsByMahasiswa(mahasiswa, month, year);
        return new LaporanPembayaran(mahasiswa, month, year, logs);
    }

    @Override
    public Log updateLog(int idLog, Log log) {
        Log oldLog = logRepository.findByIdLog(idLog);
        oldLog.setDesc(log.getDesc());
        oldLog.setStartTime(log.getStartTime());
        oldLog.setEndTime(log.getEndTime());
        return logRepository.save(oldLog);
    }

    @Override
    public void deleteLogByIdLog(Integer idLog) {
        logRepository.deleteById(idLog);
    }

    public Date convertToDateViaSqlDate(LocalDate dateToConvert) {
        return java.sql.Date.valueOf(dateToConvert);
    }
}
