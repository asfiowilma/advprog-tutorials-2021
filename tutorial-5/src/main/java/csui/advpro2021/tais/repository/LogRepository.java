package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface LogRepository extends JpaRepository<Log, Integer> {
    Log findByIdLog(Integer idLog);
    List<Log> findByMahasiswa(Mahasiswa mahasiswa);
    List<Log> findByStartTimeBetween(Date startTime, Date startTime2);
}
