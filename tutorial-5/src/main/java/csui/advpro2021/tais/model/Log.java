package csui.advpro2021.tais.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false, columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idLog;

    @Column(name = "startTime")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date startTime;

    @Column(name = "endTime")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date endTime;

    @Column(name = "description")
    private String desc;

    @ManyToOne
    @JoinColumn(name="mahasiswa", nullable=false)
    @JsonIgnore
    private Mahasiswa mahasiswa;

    @JsonGetter("mahasiswa")
    public String getJsonMataKuliah() {
        return mahasiswa == null ? "" : mahasiswa.getNpm();
    }

    public Log(Date startTime, Date endTime, String desc, Mahasiswa mahasiswa) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.desc = desc;
        this.mahasiswa = mahasiswa;
    }
}
