package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Data
public class LaporanPembayaran {
    Mahasiswa mahasiswa;
    int month;
    int year;
    double workHours;
    double reward;
    List<Log> logs;

    public LaporanPembayaran(Mahasiswa mahasiswa, int month, int year, List<Log> logs) {
        this.mahasiswa = mahasiswa;
        this.month = month;
        this.year = year;
        this.logs = logs;

        this.workHours = sumWorkHours(logs) / 60;
        this.reward = sumTotalHonor(workHours);
    }

    private long sumWorkHours(List<Log> logs) {
        int workHours = 0;
        for (Log log : logs) {
            long diff = log.getEndTime().getTime() - log.getStartTime().getTime();
            workHours += TimeUnit.MILLISECONDS.toMinutes(diff);
        }
        return workHours;
    }

    private double sumTotalHonor(double workHours) {
        return workHours * 350;
    }

    @JsonGetter("mahasiswa")
    public String getJsonLaporanPembayaran() {
        return mahasiswa == null ? "" : mahasiswa.getNpm();
    }
}
