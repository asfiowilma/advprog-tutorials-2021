package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LogServiceImplTest {

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2021, 1, 1, 4, 0, 0);
        Date end = calendar.getTime();

        log = new Log();
        log.setIdLog(1);
        log.setMahasiswa(mahasiswa);
        log.setStartTime(start);
        log.setEndTime(end);
        log.setDesc("lalalala");
    }

    @Test
    public void testServiceCreateLog() {
        lenient().when(logService.createLog(log)).thenReturn(log);
    }

    @Test
    public void testLogServiceGetListLog() {
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void testServiceGetLogByIdLog() {
        lenient().when(logService.getLogByIdLog(1)).thenReturn(log);
        Log resultLog = logService.getLogByIdLog(log.getIdLog());
        assertEquals(log.getIdLog(), resultLog.getIdLog());
    }

    @Test
    public void testLogDeleteLog() {
        logService.createLog(log);
        logService.deleteLogByIdLog(log.getIdLog());
        lenient().when(logService.getLogByIdLog(log.getIdLog())).thenReturn(null);
        assertEquals(null, logService.getLogByIdLog(log.getIdLog()));
    }

    @Test
    public void testServiceUpdateLog() {
        logService.createLog(log);
        String currentDesc = log.getDesc();
        log.setDesc("AAAAAAAAAAAAAAAAAAAAAA");

        when(logRepository.findByIdLog(log.getIdLog())).thenReturn(log);
        lenient().when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);
        Log resultLog = logService.updateLog(log.getIdLog(), log);

        assertNotEquals(resultLog.getDesc(), currentDesc);
        assertEquals(resultLog.getEndTime(), log.getEndTime());
    }

    @Test
    public void testGetMonthlyLogsByMahasiswa() {
        logService.createLog(log);
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);

        LocalDate init = LocalDate.of(2021, 1, 2);
        LocalDate start = init.withDayOfMonth(1);
        LocalDate end = init.withDayOfMonth(init.lengthOfMonth());
        when(logRepository.findByStartTimeBetween(java.sql.Date.valueOf(start), java.sql.Date.valueOf(end))).thenReturn(listLog);

        lenient().when(logService.getMonthlyLogsByMahasiswa(mahasiswa, 1, 2021)).thenReturn(listLog);
        List<Log> resultListLog = logService.getMonthlyLogsByMahasiswa(log.getMahasiswa(), 1, 2021);
        assertEquals(listLog, resultListLog);

        LaporanPembayaran laporanPembayaran = new LaporanPembayaran(mahasiswa, 1, 2021, listLog);
        LaporanPembayaran resultLaporanPembayaran = logService.getLaporanPembayaranByMahasiswa(mahasiswa, 1, 2021);
        assertEquals(laporanPembayaran, resultLaporanPembayaran);
    }

    @Test
    public void testGetAllLogsByMahasiswa() {
        logService.createLog(log);
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);
        lenient().when(logService.getAllLogsByMahasiswa(mahasiswa)).thenReturn(listLog);
        List<Log> resultListLog = logService.getAllLogsByMahasiswa(log.getMahasiswa());
        assertEquals(listLog, resultListLog);
    }

//    @Test
//    public void testLaporanPembayaranByMahasiswa() {
//        logService.createLog(log);
//        List<Log> listLog = new ArrayList<>();
//        listLog.add(log);
//        LaporanPembayaran laporanPembayaran = new LaporanPembayaran(mahasiswa, 1, 2021, listLog);
//
//        LocalDate init = LocalDate.of(2021, 1, 2);
//        LocalDate start = init.withDayOfMonth(1);
//        LocalDate end = init.withDayOfMonth(init.lengthOfMonth());
//        when(logRepository.findByStartTimeBetween(java.sql.Date.valueOf(start), java.sql.Date.valueOf(end))).thenReturn(listLog);
//
//        lenient().when(logService.getMonthlyLogsByMahasiswa(mahasiswa, 1, 2021)).thenReturn(listLog);
//        lenient().when(logService.getLaporanPembayaranByMahasiswa(mahasiswa, 1, 2021)).thenReturn(laporanPembayaran);
//        LaporanPembayaran resultLaporanPembayaran = logService.getLaporanPembayaranByMahasiswa(mahasiswa, 1, 2021);
//        assertEquals(laporanPembayaran, resultLaporanPembayaran);
//    }

}