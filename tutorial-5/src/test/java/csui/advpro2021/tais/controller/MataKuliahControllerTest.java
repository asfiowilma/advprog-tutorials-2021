package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = MataKuliahController.class)
class MataKuliahControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    private MataKuliah matkul;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
        mahasiswa = new Mahasiswa("123","litha","litha@litha.com","4.2","123");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetListMataKuliah() throws Exception {
        Iterable<MataKuliah> listMataKuliah = Arrays.asList(matkul);
        when(mataKuliahService.getListMataKuliah()).thenReturn(listMataKuliah);

        mvc.perform(get("/mata-kuliah").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].kodeMatkul").value(matkul.getKodeMatkul()));
    }

    @Test
    void testControllerCreateMataKuliah() throws Exception {
        when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);

        mvc.perform(post("/mata-kuliah")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(matkul)))
                .andExpect(jsonPath("$.kodeMatkul").value(matkul.getKodeMatkul()));
    }

    @Test
    void testControllerGetMataKuliah() throws Exception {
        when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        mvc.perform(get("/mata-kuliah/" + matkul.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value(matkul.getNama()));
    }

    @Test
    public void testControllerGetNonExistMataKuliah() throws Exception{
        mvc.perform(get("/mata-kuliah/BASDEAD").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

//    @Test
//    public void testRegisterAsdos() throws Exception{
////        mataKuliahService.createMataKuliah(matkul);
////        when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
////        when(mahasiswaService.daftarAsdos(mahasiswa, matkul)).thenReturn(mahasiswa);
////        mvc.perform(post("/mata-kuliah/BASDEAD/register")
////                .contentType(MediaType.APPLICATION_JSON_VALUE)
////                .content(mapToJson(mahasiswa)))
////                .andExpect(jsonPath("$.npm").value(mahasiswa.getNpm()));
//
//        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
//        when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
//        when(mahasiswaService.daftarAsdos(mahasiswa, matkul)).thenReturn(mahasiswa);
//
//        mvc.perform(post("/mata-kuliah/" + matkul.getKodeMatkul() + "/register")
//                .contentType(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.npm").value(mahasiswa.getNpm()));
//
//
//    }

    @Test
    public void testControllerRegisterAsdos() throws Exception {
        mataKuliahService.createMataKuliah(matkul);
        when(mataKuliahService
                .getMataKuliah(matkul.getKodeMatkul()))
                .thenReturn(matkul);
        mahasiswaService.createMahasiswa(mahasiswa);
        when(mahasiswaService
                .daftarAsdos(mahasiswa, matkul))
                .thenReturn(mahasiswa);

        mvc.perform(post("/mata-kuliah/"+ matkul.getKodeMatkul() +"/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"npm\" : \"123\",\n" +
                        "  \"nama\" : \"litha\",\n" +
                        "  \"email\" : \"litha@litha.com\",\n" +
                        "  \"ipk\" : \"4.2\",\n" +
                        "  \"noTelp\" : \"123\"\n" +
                        "}")
        ).andExpect(jsonPath("$.npm").value(mahasiswa.getNpm()));

        mvc.perform(post("/mata-kuliah/lala/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"npm\" : \"123\",\n" +
                        "  \"nama\" : \"litha\",\n" +
                        "  \"email\" : \"litha@litha.com\",\n" +
                        "  \"ipk\" : \"4.2\",\n" +
                        "  \"noTelp\" : \"123\"\n" +
                        "}")
        ).andExpect(status().isNotFound());
    }



    @Test
    void testControllerUpdateMataKuliah() throws Exception {
        mataKuliahService.createMataKuliah(matkul);

        String namaMatkul = "ADV125YIHA";
        matkul.setNama(namaMatkul);

        when(mataKuliahService.updateMataKuliah(matkul.getKodeMatkul(), matkul)).thenReturn(matkul);

        mvc.perform(put("/mata-kuliah/" + matkul.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(matkul)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value(namaMatkul));
    }

    @Test
    void testControllerDeleteMataKuliah() throws Exception {
        mataKuliahService.createMataKuliah(matkul);
        mvc.perform(delete("/mata-kuliah/" + matkul.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}