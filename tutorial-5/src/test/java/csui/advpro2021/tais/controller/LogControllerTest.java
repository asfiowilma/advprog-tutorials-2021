package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    private Log log;
    private Mahasiswa mahasiswa;
    private MataKuliah matkul;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("123213");
        mahasiswa.setNama("Lalalala");
        mahasiswa.setEmail("lala@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2021, 1, 1, 4, 0, 0);
        Date end = calendar.getTime();

        log = new Log();
        log.setIdLog(1);
        log.setMahasiswa(mahasiswa);
        log.setStartTime(start);
        log.setEndTime(end);
        log.setDesc("lalalala");

        matkul = new MataKuliah("SDB", "A", "A");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetListLog() throws Exception {
        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value(log.getIdLog()));
    }

    @Test
    public void testControllerPostLog() throws Exception {
        when(logService.createLog(any(Log.class))).thenReturn(log);

        mvc.perform(post("/log/asdos/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(log)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idLog").value(1));
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        when(logService.updateLog(eq(1), any(Log.class))).thenReturn(log);

        mvc.perform(put("/log/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(log)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idLog").value(1));
    }

    @Test
    public void testControllerDeleteLog() throws Exception {
        mvc.perform(delete("/log/" + log.getIdLog()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerGetLogByIdLog() throws Exception {
        when(logService.getLogByIdLog(1)).thenReturn(log);

        mvc.perform(get("/log/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.idLog").value(1));
        mvc.perform(get("/log/234")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetLogByMahasiswa() throws Exception {
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.getAllLogsByMahasiswa(mahasiswa)).thenReturn(listLog);

        mvc.perform(get("/log/asdos/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value(1));
        mvc.perform(get("/log/asdos/1221213232")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetMonthlyLogByMahasiswa() throws Exception {
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.getMonthlyLogsByMahasiswa(mahasiswa, 1, 2021)).thenReturn(listLog);

        mvc.perform(get("/log/asdos/" + mahasiswa.getNpm() +"/1-2021")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value(1));
        mvc.perform(get("/log/asdos/1221213232/1-2021")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetLaporanPembayaranByMahasiswa() throws Exception {
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);
        LaporanPembayaran laporanPembayaran = new LaporanPembayaran(mahasiswa, 1, 2021, listLog);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.getLaporanPembayaranByMahasiswa(mahasiswa, 1, 2021)).thenReturn(laporanPembayaran);

        mvc.perform(get("/log/asdos/" + mahasiswa.getNpm() +"/laporan/1-2021")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.month").value(1));
        mvc.perform(get("/log/asdos/1221213232/laporan/1-2021")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        assertEquals(mahasiswa.getNpm(), laporanPembayaran.getJsonLaporanPembayaran());
        LaporanPembayaran lapem = new LaporanPembayaran(null, 1, 2021, listLog);
        assertEquals("", lapem.getJsonLaporanPembayaran());

        assertEquals(mahasiswa.getNpm(), log.getJsonMataKuliah());
        Log log = new Log(null, null, null, null);
        assertEquals("", log.getJsonMataKuliah());
    }
}
