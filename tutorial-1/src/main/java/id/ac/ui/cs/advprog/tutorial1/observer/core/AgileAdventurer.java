package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    @Override
    public void update() {
        Quest q = guild.getQuest();
        String type = guild.getQuestType();
        if (type.equals("D") || type.equals("R")) {
            this.getQuests().add(q);
        }
    }
}
