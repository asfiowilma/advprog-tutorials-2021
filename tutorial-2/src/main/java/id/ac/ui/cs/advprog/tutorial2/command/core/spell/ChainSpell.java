package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ChainSpell implements Spell {
    List<Spell> spells;
    List<Spell> reversedSpells;

    public ChainSpell(List<Spell> spells) {
        this.spells = spells;
        this.reversedSpells = spells;
        Collections.reverse(reversedSpells);
    }

    @Override
    public void cast() {
        for (Spell spell: spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (Spell spell : reversedSpells) {
            spell.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
